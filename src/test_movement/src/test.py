#!/usr/bin/env python3

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

# Inspired from http://docs.ros.org/kinetic/api/moveit_tutorials/html/doc/move_group_python_interface/move_group_python_interface_tutorial.html
# Modified by Alexandre Vannobel to test the FollowJointTrajectory Action Server for the Kinova Gen3 robot

# To run this node in a given namespace with rosrun (for example 'my_gen3'), start a Kortex driver and then run : 
# rosrun kortex_examples example_moveit_trajectories.py __ns:=my_gen3

import sys
import time
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from moveit_msgs.msg import RobotTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint


from math import pi
from std_srvs.srv import Empty

class ExampleMoveItTrajectories(object):
	"""ExampleMoveItTrajectories"""
	def __init__(self):

		# Initialize the node
		super(ExampleMoveItTrajectories, self).__init__()
		moveit_commander.roscpp_initialize(sys.argv)
		rospy.init_node('example_move_it_trajectories')

		try:
			self.is_gripper_present = rospy.get_param(rospy.get_namespace() + "is_gripper_present", False)
			if self.is_gripper_present:
				gripper_joint_names = rospy.get_param(rospy.get_namespace() + "gripper_joint_names", [])
				self.gripper_joint_name = gripper_joint_names[0]
			else:
				self.gripper_joint_name = ""
			self.degrees_of_freedom = rospy.get_param(rospy.get_namespace() + "degrees_of_freedom", 7)

			# Create the MoveItInterface necessary objects
			arm_group_name = "arm"
			self.robot = moveit_commander.RobotCommander("robot_description")
			self.scene = moveit_commander.PlanningSceneInterface(ns=rospy.get_namespace())
			self.arm_group = moveit_commander.MoveGroupCommander(arm_group_name, ns=rospy.get_namespace())
			self.display_trajectory_publisher = rospy.Publisher(rospy.get_namespace() + 'move_group/display_planned_path',
																										moveit_msgs.msg.DisplayTrajectory,
																										queue_size=20)

			if self.is_gripper_present:
				gripper_group_name = "gripper"
				self.gripper_group = moveit_commander.MoveGroupCommander(gripper_group_name, ns=rospy.get_namespace())

			rospy.loginfo("Initializing node in namespace " + rospy.get_namespace())
		except Exception as e:
			print (e)
			self.is_init_success = False
		else:
			self.is_init_success = True

	def test_traj(self): # Can delete once working
		arm_group = self.arm_group

		rospy.loginfo("going to starting position")
		joints = [0.14326741400190268, -0.14454200289961427, 2.210325700437653, -2.5811096295261144, 1.939946140238486, -0.17301279284921645]
		arm_group.go(joints, wait=True)	

		trajectory = RobotTrajectory()
		trajectory.joint_trajectory.header.frame_id = "base_link"
		trajectory.joint_trajectory.joint_names = [
			'joint_1',
			'joint_2',
			'joint_3',
			'joint_4',
			'joint_5',
			'joint_6'
		]
		trajectory.joint_trajectory.points = []


		j_point = JointTrajectoryPoint()
		j_point.positions = [0.14325715251489968, -0.14441256344306366, 2.2102757288569537, -2.5809500433148744, 1.9397525149416734, -0.1730436913150741]
		j_point.velocities = [-0.000724363929957437, 0.009137201402789355, -0.0035275209695156753, 0.011265277157879783, -0.01366811465992214, -0.002181139453936675]
		j_point.accelerations = [-0.03408882831307224, 0.42999999999995475, -0.1660064115945773, 0.5301480140744854, -0.6432264152534142, -0.10264521091834689]
		j_point.time_from_start = rospy.Time(0, 42498611)
		trajectory.joint_trajectory.points.append(j_point)

		j_point = JointTrajectoryPoint()
		j_point.positions = [0.14048669666144897, -0.10946574725909933, 2.196784111295665, -2.537864031226138, 1.8874764328520581, -0.18138583856653473]
		j_point.velocities = [-0.013761772378056876, 0.17359241767469155, -0.06701733567022439, 0.2140225011363596, -0.25967262450251427, -0.0414382100605727]
		j_point.accelerations = [-0.034082855353790596, 0.4299246564748502, -0.1659773243660313, 0.5300551228646786, -0.6431137107288782, -0.10262722568107738]
		j_point.time_from_start = rospy.Time(0, 424986112)
		trajectory.joint_trajectory.points.append(j_point)

		j_point = JointTrajectoryPoint()
		j_point.positions = [0.13770597932099524, -0.07438949161858438, 2.1832425221536775, -2.494618432926162, 1.8350067254656302, -0.18975888428385304]
		j_point.velocities = [-0.01946597393136392, 0.24554580502479448, -0.09479576273083762, 0.3027340020889032, -0.367305925457291, -0.05861418824857645]
		j_point.accelerations = [-0.034078837555674375, 0.42987397555457096, -0.1659577584179504, 0.5299926382386015, -0.643037898387765, -0.10261512764909804]
		j_point.time_from_start = rospy.Time(0, 592358761)
		trajectory.joint_trajectory.points.append(j_point)

		try:
			arm_group.execute(trajectory, wait=True)
			return True
		except:
			return False

def main():
	example = ExampleMoveItTrajectories()

	# For testing purposes
	success = example.is_init_success
	try:
			rospy.delete_param("/kortex_examples_test_results/moveit_general_python")
	except:
			pass

	if success:
		rospy.loginfo("Attempting waypoints")
		success &= example.test_traj()
		print (success)	

	if not success:
			rospy.logerr("The example encountered an error.")

if __name__ == '__main__':
	main()
